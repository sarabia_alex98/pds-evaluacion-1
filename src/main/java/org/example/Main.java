package org.example;

import org.example.services.CarServices;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CarServices carServices = new CarServices();

        int opcion = 0;

        System.out.println("¿Usted es agente?");
        System.out.println("[1] Sí");
        System.out.println("[2] No");
        System.out.print("Ingrese una opción: ");
        opcion = scanner.nextInt();

        if(opcion == 1) {
            carServices.generateCars(10);
        }
        System.out.println("\nAutomóviles disponibles:");
        carServices.showCars();

        System.out.println("\nFiltrar automóviles por:");
        System.out.println("[1] Precio");
        System.out.println("[2] Tipo");
        System.out.println("[3] Color");
        System.out.print("Ingrese una opción: ");
        int opcionFiltro = scanner.nextInt();
        switch (opcionFiltro) {
            case 1:
                System.out.println("\nIngrese el precio minimo: ");
                System.out.print("\nIngrese el precio máximo: ");
                int precioMin = scanner.nextInt();
                int precioMax = scanner.nextInt();

                carServices.filtrarPorPrecio(precioMin, precioMax);
                break;
            case 2:
                System.out.print("\nIngrese el tipo de automóvil (Sedan, Camioneta o SUV): ");
                String tipo = scanner.next();
                carServices.filtrarPorTipo(tipo);
                break;
            case 3:
                System.out.print("\nIngrese el color de automóvil: ");
                String color = scanner.next();
                carServices.filterByColor(color);
                break;
            default:
                System.out.println("\nOpción inválida.");
                break;
        }
    }

    }
