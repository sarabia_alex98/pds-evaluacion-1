package org.example.services;

import org.example.model.Car;

import java.util.Random;

public class CarServices {
    private final Random random = new Random();
    private Car[] cars;
    private final String[] yearsOfManufacture = {"2015","2016","2017","2018","2019","2020","2021","2022","2023"};
    private final String[] brands = {"Zuzuki","Toyota","Nissan","Subaru","BMW"};
    private final String[] colors = {"Azul","Rojo","Verde","Amarillo","Negro"};
    private final String [] types = {"Sedán", "Camioneta", "SUV"};

    //Motores
    private final String[] sedanEngines = {"1.4cc", "1.6cc", "2.0cc"};
    private final String[] pickupTruckEngines = {"2.4cc", "3.0cc", "4.0cc"};
    private final String[] suvEngines = {"1.8cc", "2.2cc", "2.8cc"};

    /**

     * Este método genera un arreglo de objetos de tipo Car con una cantidad específica de automóviles.

     * @param numberOfCars la cantidad de automóviles que se quiere generar

     * @return un arreglo de objetos de tipo Car con la cantidad de automóviles especificada
     */

    public Car[] generateCars(int numberOfCars){
        this.cars = new Car[numberOfCars];
        for (long i = 0; i < cars.length; i++) {
            Car car = new Car();
            car.setId(i+1);
            generateCar(car);
            cars[(int)i] = car;
        }
        return cars;
    }

    /**

     * Genera un automóvil con características aleatorias, como tipo de vehículo, motor, cabina, año de elaboración,
       color, marca, si tiene o no sunroof y si tiene o no turbo.

     * Además, establece su precio y popularidad en cero.

     * @param car objeto Car en el que se guardarán las características generadas del automóvil
     */

    private void generateCar(Car car) {
        int tipoVehiculoAleatorio = random.nextInt(3);
        if(tipoVehiculoAleatorio==0){
            car.setTipo(types[0]);
        }else if(tipoVehiculoAleatorio==1){
            car.setTipo(types[1]);
        }else{
            car.setTipo(types[2]);
            if(random.nextInt(2)==0){
                car.setSunroof(true);
            }else{
                car.setSunroof(false);
            }
        }
        car.setMotor(elegirMotor(car.getTipo()));
        car.setCabina(generateCabins(car.getTipo()));
        car.setAnioElaboración(yearsOfManufacture[random.nextInt(9)]);
        car.setColor(colors[random.nextInt(5)]);
        car.setMarca(brands[random.nextInt(5)]);
        int turbo= random.nextInt(2);
        if(turbo==0){
            car.setTurbo(false);
        }else{
            car.setTurbo(true);
        }
        car.setPopularidad(0);
        car.setPrecio(random.nextInt(22000001) + 8000000);
    }

    /**

     * Genera el número de cabinas de un automóvil dependiendo del tipo.
     * Si el tipo es "Camioneta", se genera un número aleatorio entre 1 y 2.
     * Si el tipo es otro, se devuelve 0.
     * @param type el tipo de automóvil
     * @return el número de cabinas generado

     */

    private int generateCabins(String type) {
        return type.equals("Camioneta") ? random.nextInt(2) + 1 : 0;
    }
    /**

     * Este método elige al azar un tipo de motor dependiendo del tipo de vehículo.
     * @param tipo el tipo de vehículo para el que se va a elegir un motor
     * @return una cadena que representa el tipo de motor elegido al azar
     */

    private String elegirMotor(String tipo) {
        int tipoMotorAleatorio = random.nextInt(3);
        if(tipo.equals("Sedán")){
            if(tipoMotorAleatorio==0){
                return sedanEngines[0];
            }
            else if(tipoMotorAleatorio==1){
                return sedanEngines[1];
            }else{
                return sedanEngines[2];
            }
        }else if(tipo.equals("Camioneta")){
            if(tipoMotorAleatorio==0){
                return pickupTruckEngines[0];
            }
            else if(tipoMotorAleatorio==1){
                return pickupTruckEngines[1];
            }else{
                return pickupTruckEngines[2];
            }
        }else{
            if(tipoMotorAleatorio==0){
                return suvEngines[0];
            }
            else if(tipoMotorAleatorio==1){
                return suvEngines[1];
            }else{
                return suvEngines[2];
            }
        }

    }

    /**

     * Despliega la información de todos los vehículos almacenados en el arreglo "cars".

     * Para cada vehículo, muestra su ID, tipo, año de elaboración, marca, color, motor,
     popularidad y precio en consola.

     * @return void

     */

    public void showCars(){
        for (int i = 0; i < cars.length; i++) {
            System.out.println(cars[i].getId()+"-"+cars[i].getTipo()+"-"+cars[i].getAnioElaboración()+"-"+cars[i].getMarca()+"-"+cars[i].getColor()+"-"+cars[i].getMotor()+"-"+cars[i].getPopularidad()+"-"+cars[i].getPrecio());
        }
    }

    /**

     * Filtra y despliega la información de los vehículos que coinciden con el color
       especificado.

     * Recorre el arreglo "cars" y muestra en consola la información de los
       vehículos cuyo color es igual al color recibido como parámetro.

     * @param color El color por el cual se van a filtrar los vehículos.

     * @return void
     */

    public void filterByColor(String color){
        for (int i = 0; i < cars.length; i++) {
            if(cars[i].getColor().equals(color)){
                System.out.println(cars[i].getId()+"-"+cars[i].getTipo()+"-"+cars[i].getAnioElaboración()+"-"+cars[i].getMarca()+"-"+cars[i].getColor()+"-"+cars[i].getMotor()+"-"+cars[i].getPopularidad()+"-"+cars[i].getPrecio());
            }
        }
    }
    /**

     * Filtra y despliega la información de los vehículos que coinciden con el tipo
       especificado.
     * Recorre el arreglo "cars" y muestra en consola la información
       de los vehículos cuyo tipo es igual al tipo recibido como parámetro.

     * @param tipo El tipo por el cual se van a filtrar los vehículos.

     * @return void
     */
    public void filtrarPorTipo(String tipo){
        for (int i = 0; i < this.cars.length; i++) {
            if(cars[i].getTipo().equals(tipo)){
                System.out.println(cars[i].getId()+"-"+cars[i].getTipo()+"-"+cars[i].getAnioElaboración()+"-"+cars[i].getMarca()+"-"+cars[i].getColor()+"-"+cars[i].getMotor()+"-"+cars[i].getPopularidad()+"-"+cars[i].getPrecio());
            }
        }
    }
    /**

     * Filtra y despliega la información de los vehículos que tienen un precio en el
       rango especificado.
     * Recorre el arreglo "cars" y muestra en consola la información
       de los vehículos cuyo precio está entre el valor mínimo y el valor máximo recibidos como parámetros.
     * @param valorMinimo El valor mínimo del rango de precios.
     * @param valorMaximo El valor máximo del rango de precios.
     * @return void

     */

    public void filtrarPorPrecio(int valorMinimo, int valorMaximo){
        for (int i = 0; i < this.cars.length; i++) {
            if(cars[i].getPrecio()>=valorMinimo && cars[i].getPrecio()<=valorMaximo){
                System.out.println(cars[i].getId()+"-"+cars[i].getTipo()+"-"+cars[i].getAnioElaboración()+"-"+cars[i].getMarca()+"-"+cars[i].getColor()+"-"+cars[i].getMotor()+"-"+cars[i].getPopularidad()+"-"+cars[i].getPrecio());
            }
        }
    }
    /**

     * Incrementa en 1 la popularidad del vehículo cuyo ID corresponde al valor recibido
     como parámetro.
     * Recorre el arreglo "cars" en busca del vehículo con el ID
       especificado y le incrementa su popularidad en 1.
     * @param idVehiculo El ID del vehículo cuya popularidad se desea incrementar.
     * @return void
     */

    public void aumentarPopularidadIndividual(Long idVehiculo){
        for (int i = 0; i < cars.length; i++) {
            if(cars[i].getId()==idVehiculo){
                cars[i].setPopularidad(cars[i].getPopularidad()+1);
                break;
            }
        }
    }
    /**

     * Incrementa en 1 la popularidad de todos los vehículos almacenados en el arreglo "cars".
     * Recorre el arreglo y le incrementa en 1 la popularidad a cada vehículo.
     * @return void
     */


    public void aumentarPopularidadColectiva(){
        for (int i = 0; i < cars.length; i++) {
            cars[i].setPopularidad(cars[i].getPopularidad()+1);
        }
    }
}
