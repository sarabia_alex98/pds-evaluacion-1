Evaluacion 1 Pruebas de Software

Este proyecto consiste en un modulo de software para una automotora, el cual nos filtrara autimoviles.

Requisitos
Java 17
Maven 3.6.3 o superior
...
Instalación
Clonar el repositorio: git clone https://gitlab.com/sarabia_alex98/pds-evaluacion-1.git
Ingresar al directorio del proyecto: cd mi-proyecto
Compilar y empaquetar el proyecto: mvn clean package
...
Uso
Se ejecuta mediante consola. 

Contribución
Se agradece cualquier tipo de contribución al proyecto. Para hacerlo, siga los siguientes pasos:

Fork del proyecto
Crear una nueva rama (git checkout -b feature/nombre-del-nuevo-feature)
Realizar cambios y hacer commit (git commit -am 'Agregado nuevo feature')
Hacer push a la rama (git push origin feature/nombre-del-nuevo-feature)
Crear una pull request
Autor
Alejandro Vera
Alex Sarabia.
Contacto: a.vera07@ufromail.cl - a.sarabia02@ufromail.cl
